import firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyACmB5gHlTQ9RyVOzqKi6i0475WXM8rlG8",
  authDomain: "agcode-263c7.firebaseapp.com",
  databaseURL: "https://agcode-263c7.firebaseio.com",
  projectId: "agcode-263c7",
  storageBucket: "agcode-263c7.appspot.com",
  messagingSenderId: "262634028191",
  appId: "1:262634028191:web:58eff5bbaee7cde3f076aa",
  measurementId: "G-ZK4L281JVX"
};

  firebase.initializeApp(firebaseConfig);
  

  export default firebase;