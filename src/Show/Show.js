import React,{useState} from 'react';
import {Text,
        StyleSheet,
        SafeAreaView,
        TextInput,
        TouchableOpacity,
        View,
        } from 'react-native';

import firebase from '../../FirebaseConfig';



export default function Show(){

  const[dispositivo,setDisp]= useState('');
  const[sensor1,setSensor1] = useState('');
  const[sensor2,setSensor2] = useState('');
  const[sensor3,setSensor3] = useState('')
 
  
  function pushFire(){
    try{
      firebase.database().ref('/Cadastro').push({
       dispositivo:dispositivo,
       sensor1:sensor1,
       sensor2:sensor2,
       sensor3:sensor3
    })
  }catch(error){
    alert(error);
  }
  finally{
    setDisp('');
    setSensor1('');
    setSensor2('');
    setSensor3('');
    }
  }

   return(

     
    <SafeAreaView style={styles.container}>
         
          <TextInput style={styles.textInput}
                onChangeText={dispositivo =>setDisp(dispositivo)} value={dispositivo}
                placeholder ="Dispositivo:"/>

          <TextInput style={styles.textInput}
                onChangeText={sensor1 =>setSensor1(sensor1)} value={sensor1}
                placeholder = "Tipo de Sensor:"/>

          <TextInput style={styles.textInput}
                onChangeText={sensor2 =>setSensor2(sensor2)} value={sensor2}
                placeholder = "Tipo de Sensor :"/> 

          <TextInput style={styles.textInput}
                onChangeText={sensor3 =>setSensor3(sensor3)} value={sensor3}
                placeholder = "Tipo de Sensor :"/>        
                
           <TouchableOpacity style={styles.btnEnviar} onPress={pushFire}>
                <Text style = {styles.text}>Enviar</Text>
           </TouchableOpacity>    
    </SafeAreaView>
     
  );
}


const styles = StyleSheet.create({

 container:{
   flex:1,
   backgroundColor:'#00004d',
   alignItems: 'center',
   justifyContent: 'center',
   marginVertical: 70,
   height:'70%'
   
   
 },
 text:{
   color:'#000'
 },
 textInput:{
  backgroundColor:"#fff",
  width:'90%',
  marginBottom:15,
  color:'#000',
  fontSize:17,
  borderRadius:10,
  padding:10,
  textAlign:'center',
  marginBottom:40

},
  btnEnviar:{
    backgroundColor:"#35AAFF",
    width:'90%',
    height:45,
    alignItems:'center',
    justifyContent:'center',
    borderRadius:7,
  }
});