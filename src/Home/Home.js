import React,{useState,useEffect} from 'react';
import {View,
        Text,
        Button,      
        StyleSheet,      
        Image,
        TouchableOpacity,
        SafeAreaView,
        Animated,
        ScrollView,
        FlatList,
        TextInput} from 'react-native';

import firebase from '../../FirebaseConfig';


export default function Home({navigation}){

  const[LED_STATUS,setStatus]= useState('');
  const[velocidade,setVelocidade]= useState('');
  
 
  
  function pushStatus(){
    try{
      firebase.database().ref('LED_STATUS').push({
        LED_STATUS:LED_STATUS,      
    })
  }catch(error){
    alert(error);
  }
  finally{
    setStatus('');   
    }
  }
  function pushVelocidade(){
    try{
      firebase.database().ref('Sensores/velocidade/').push({
        velocidade:velocidade,      
    })
  }catch(error){
    alert(error);
  }
  finally{
    setVelocidade('');   
    }
  }


  function logOutFirebase(){
    firebase.auth().signOut().then(function() {
      alert('Deslogado com sucesso"')
      this.props.navigation.navigate('index')
    }).catch(function(error) {
      alert('Falha"')
    });
  }
  const[listFire,setListFire] = useState(null);
  const[listSensor,setListSensor] = useState(null);
  const[listStatus,setListStatus] = useState(null);
  

  function delFire(key) {
    firebase.database().ref('/Cadastro/' + key).remove()
  }





  useEffect(()=>{
    try{
        firebase.database().ref('/Cadastro').on('value',(snapshot)=>{
        const list =[];
        snapshot.forEach(childItem=>{
            list.push({
              key:childItem.key,
              dispositivo:childItem.val().dispositivo,
              sensor1:childItem.val().sensor1,
              sensor2:childItem.val().sensor2,
              sensor3:childItem.val().sensor3
         });
      });
       setListFire(list);
    })
  }catch(error){
    alert(error);
  }
  },[]) 

  
  useEffect(()=>{
    try{
        firebase.database().ref('/LED_STATUS').on('value',(snapshot)=>{
        const listStatus =[];
        snapshot.forEach(childItem=>{
          listStatus.push({
              key:childItem.key,
              LED_STATUS:childItem.val().LED_STATUS,              
         });
      });
       setListStatus(listStatus);
    })
  }catch(error){
    alert(error);
  }
  },[]) 
  



  useEffect(()=>{
    try{
        firebase.database().ref('/Sensores').on('value',(snapshot)=>{
        const listSensor =[];
        snapshot.forEach(childItem=>{
          listSensor.push({
              key:childItem.key,              
              Humidade:childItem.val().Humidade, 
              Temperatura:childItem.val().Temperatura,
              Velocidade:childItem.val().Velocidade        
         });
      });
       setListSensor(listSensor);
    })
  }catch(error){
    alert(error);
  }
  },[]) 


  const[scrollY,setScrollY]= useState(new Animated.Value(0));
  return(
    <SafeAreaView style={styles.backgound}>
     <Animated.View 
       style={[
       styles.header,
       {
         height: scrollY.interpolate({
           inputRange:[10,160,185],
           outputRange:[140,20,0],
           extrapolate:'clamp'
         }),
         opacity: scrollY.interpolate({
           inputRange:[1,75,170],
           outputRange:[1,1,0],
           extrapolate: 'clamp'
         })
       }
       ]}>
       
      <TouchableOpacity style={styles.btnsair} onPress={()=>{logOutFirebase()}}>
        <Image
          style={{
            width: 40,
            height:40,
            position:"absolute"             
             }}
         source={require('../assets/image/sair.png')}
        />
        
      </TouchableOpacity> 
           
         
 

          <Animated.Image
          source={require('../assets/image/logo.png')}
          style={{
            width:scrollY.interpolate({
              inputRange:[0,120],
              outputRange:[200,120],
              extrapolate:'clamp'
            }),
            height:30
          }}
          resizeMode="contain"
          />
          <Image
          source={require('../assets/image/config.png')}
          style={{width:30,height:30}}
          resizeMode="contain"
          />
     </Animated.View>

       <ScrollView
         scrollEventThrottle={16}
         onScroll={Animated.event([{
         nativeEvent:{
               contentOffset:{ y:scrollY}
                     },
         }],
         {useNativeDriver:false})}>


     <View style ={styles.box}>
          <TouchableOpacity style={styles.button} onPress={()=>navigation.navigate('Cadastro')}>
               <Text style={styles.text_cadastro}>Cadastro</Text>
          </TouchableOpacity> 

          <TouchableOpacity style={styles.buttonEdit} onPress={()=>navigation.navigate('Editar')}>
               <Text style={styles.text_cadastro}>Editar</Text>
          </TouchableOpacity>  

            <TouchableOpacity style={styles.buttonExcluir} onPress={delFire}>
               <Text style={styles.text_cadastro}>Excluir</Text>
          </TouchableOpacity>                    
     </View>


      <View style ={styles.box}>
          <FlatList style={styles.viewFlat} data={listFire}
            keyExtractor={(item) => item.key}
            renderItem={({ item }) =>
            <View style={styles.iconFlat}>
               <Text style={styles.textfire}> dispositivo: {item.dispositivo} </Text>
               <Text style={styles.textfire}> sensor1: {item.sensor1}</Text> 
               <Text style={styles.textfire}> sensor2: {item.sensor2}</Text>
               <Text style={styles.textfire}> sensor3: {item.sensor3}</Text>    
            </View>
          }/>

        
       

          <TouchableOpacity style={styles.buttonStart} onPress={pushStatus}>
               <TextInput  style={styles.text_start} onChangeText={LED_STATUS =>setStatus(LED_STATUS)} value={LED_STATUS}></TextInput>
         </TouchableOpacity> 

          <TouchableOpacity style={styles.buttonStop}>
               <Text style={styles.text_stop}>STOP</Text>
          </TouchableOpacity>  

          <TouchableOpacity style={styles.buttonSpeed} onPress={pushVelocidade}>
          <Text style={styles.text_speed}>+</Text>
         </TouchableOpacity>   

          <TouchableOpacity style={styles.buttonLow}>
               <Text style={styles.text_low}>-</Text>
          </TouchableOpacity> 
      </View>  




        <View style ={styles.box}> 
           <FlatList style={styles.viewFlat2} data={listSensor}
              keyExtractor={(item) => item.key}
              renderItem={({ item }) =>
              <View style={styles.iconFlat2}>              
                 <Text style={styles.textfire2}> Humidade: {item.Humidade}</Text> 
                 <Text style={styles.textfire2}> Temperatura: {item.Temperatura}</Text>
                 <Text style={styles.textfire2}> Velocidade: {item.Velocidade}</Text>             
              </View>
           }/>

       <FlatList style={styles.viewFlat2} data={listStatus}
            keyExtractor={(item) => item.key}
            renderItem={({ item }) =>
            <View style={styles.iconFlat2}>
               <Text style={styles.textstatus}> LED_STATUS: {item.LED_STATUS}</Text>                 
            </View>
          }/>
      </View>




      <View style ={styles.box}>

     

      </View>

      <View style ={styles.box}>




      </View>
      

     </ScrollView>
   </SafeAreaView>   
      
     );
  }


  const styles = StyleSheet.create({
   backgound:{
   backgroundColor:'#00004d'
   
   },   
    header:{
    backgroundColor:'#2D3579',  
    flexDirection: 'row',
    alignItems:'center',
    justifyContent:'space-between',
    borderBottomWidth: 2,
    paddingLeft:10,
    paddingRight: 10,
    borderColor:'#fff'
   },
   box:{
     height:300,
     backgroundColor:'#fff',
     margin:7,
     borderRadius:5
   },
   iconFlat: {
    flexDirection: 'row',
    width: 350,
    height: 50,
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5
  },

  iconFlat2: {
    flexDirection: 'row',
    width: 350,
    height: 50,
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5    
  },

  viewFlat: {
    maxHeight: 150,
  },
  viewFlat2: {
    maxHeight: 200,    
  },
   button:{
     backgroundColor:'#2D3579',
     borderRadius:20,
     height:100,
     width:100,
     justifyContent:'space-between',
     position:"absolute",
     flexDirection:'row',
     marginVertical:100,
     marginHorizontal:20
     
   },
   buttonEdit:{
    backgroundColor:'#2D3579',
    borderRadius:20,
    height:100,
    width:100,
    justifyContent:'space-between',
    position:"absolute",
    flexDirection:'row',
    marginVertical:100,
    marginHorizontal:150   
  },

  buttonExcluir:{
    backgroundColor:'#2D3579',
    borderRadius:20,
    height:100,
    width:100,
    justifyContent:'space-between',
    position:"absolute",
    flexDirection:'row',
    marginVertical:100,
    marginHorizontal:280  
  },
   buttonStart:{
    backgroundColor:'#009933',
    borderRadius:50,
    height:80,
    width:80,
    justifyContent:'space-between',
    position:"absolute",
    flexDirection:'row',
    marginVertical:150,
    marginHorizontal:10
    
  },
  buttonStop:{
    backgroundColor:'#ff6666',
    borderRadius:50,
    height:80,
    width:80,
    justifyContent:'space-between',
    position:"absolute",
    flexDirection:'row',
    marginVertical:150,
    marginHorizontal:100   
  },

  buttonSpeed:{
    backgroundColor:'#6666ff',
    borderRadius:50,
    height:80,
    width:80,
    justifyContent:'space-between',
    position:"absolute",
    flexDirection:'row',
    marginVertical:150,
    marginHorizontal:200  
  },
 
  buttonLow:{
    backgroundColor:'#6666ff',
    borderRadius:50,
    height:80,
    width:80,
    justifyContent:'space-between',
    position:"absolute",
    flexDirection:'row',
    marginVertical:150,
    marginHorizontal:300  
  },

   text_start:{
     marginHorizontal:25,
     marginVertical:35,
     textAlign:'center',
     color:'#fff',
     position:'absolute',
     fontSize:10    
   },
   text_speed:{
    marginHorizontal:34,
    marginVertical:25,
    textAlign:'center',
    color:'#fff',
    position:'absolute',
    fontSize:25     
  },
  text_low:{
    marginHorizontal:37,
    marginVertical:20,
    textAlign:'center',
    color:'#fff',
    position:'absolute',
    fontSize:32     
  },
   
   text_stop:{
    marginHorizontal:25,
    marginVertical:35,
    textAlign:'center',
    color:'#fff',
    position:'absolute',
    fontSize:10     
  },

  
  text_cadastro:{
    marginHorizontal:20,
    marginVertical:38,
    textAlign:'center',
    color:'#fff',
    position:'absolute',
    fontSize:15     
  },

   textfire:{
     fontSize:15,
     color:'#000',
     textAlign:'center',
     flex:3,
     justifyContent:"space-around"
    },

    textstatus:{
      fontSize:10,
      color:'red',
      flex:3,      
      position:"absolute",
      marginHorizontal:100,
      marginVertical:20
         },
         
   btnsair:{
     width:80,
     height:50,
     backgroundColor:'#2D3579',
     borderRadius:25,
     fontSize:12,
     color:'#fff'
     
   }
  });