// components/login.js

import React,{ Component,useState,useEffect} from 'react';
import { StyleSheet,
         KeyboardAvoidingView,
         Text,
         View,
         TextInput,
         Button,
         Alert,
         ActivityIndicator,
         TouchableOpacity,
         ImageBackground,
         Animated,
         Image} from 'react-native';


import firebase from '../../FirebaseConfig';




export default class Login extends Component {
  
  constructor() {
    super();
    this.state = { 
      email: '', 
      password: '',
      isLoading: false
    }
  }

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  userLogin = () => {
    if(this.state.email === '' && this.state.password === '') {
      Alert.alert('Enter details to signin!')
    } else {
      this.setState({
        isLoading: true,
      })
      firebase
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        console.log(res)
        console.log('User logged-in successfully!')
        this.setState({
          isLoading: false,
          email: '', 
          password: ''
        })
        this.props.navigation.navigate('Home')
      })
      .catch(error => this.setState({ errorMessage: error.message }))
    }
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }    
   return (
     
     <KeyboardAvoidingView style={styles.background}>  
         
          <ImageBackground
		      style={{
			    width: "100%",
			    height:"100%",
			    position:"absolute",	 
        	 }}
		      source={require('../assets/image/background.png')}
		  	/>

      <View style={styles.logo}>
			    <Image
			  	source={require('../assets/image/logo.png')}
			    />
			</View>

      
      <Animated.View style={styles.container}>  
           <TextInput
           style={styles.inputStyle}
           placeholder="Email"
           value={this.state.email}
           onChangeText={(val) => this.updateInputVal(val, 'email')}
           />
        <TextInput
          style={styles.inputStyle}
          placeholder="Password"
          value={this.state.password}
          onChangeText={(val) => this.updateInputVal(val, 'password')}
          maxLength={15}
          secureTextEntry={true}
        />   
      		<TouchableOpacity style = {styles.btnacessar}  onPress={() => this.userLogin()}>
						<Text style = {styles.textbtn}>Acessar</Text>
		  		</TouchableOpacity>	   
      </Animated.View>
    </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  background:{
    flex:1,
    alignItems:'center',
    justifyContent:'center'	
  },
  container: {
  	flex:1,
		alignItems:"center",
		justifyContent:"center",
		width:"90%",
		paddingBottom:60
  },

  inputStyle: {    
      backgroundColor:"#5B80C7",
      width:'90%',
      marginBottom:15,
      color:'#fff',
      fontSize:17,
      borderRadius:10,
      padding:10,
      textAlign:'center',
      position:'relative'
      },
      textbtn:{
        color:'#fff',
        fontSize:18
      },

  loginText: {
    color: '#3740FE',
    marginTop: 25,
    textAlign: 'center'
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  btnacessar:{
    backgroundColor:"#35AAFF",
    width:'90%',
    height:45,
    alignItems:'center',
    justifyContent:'center',
    borderRadius:7,
   },
   logo:{
     position:'relative',
     flex:1,
     justifyContent:'center',	 	
    }
});