import React,{useState} from 'react';
import {View,
        Text,
        Button,      
        StyleSheet,      
        Image,
        TouchableOpacity,
        SafeAreaView,
        Animated,
        ScrollView,
        KeyboardAvoidingView} from 'react-native';
import Show from '../Show/Show';

export default function Cadastro(){
  return(
      <SafeAreaView style={styles.backgound}>
        <View style={styles.header}>
          <Image
          source={require('../assets/image/menu.png')}
          style={{width:40,height:40}}
          resizeMode="contain"
          />
          
           <Image
          source={require('../assets/image/logo.png')}
          style={{width:50,height:150}}
          resizeMode="contain"
          />

          <Image
          source={require('../assets/image/config.png')}
          style={{width:40,height:40}}
          resizeMode="contain"
          />            
        </View>
        <Show/>
      </SafeAreaView>
  );

}



const styles = StyleSheet.create({

    backgound:{
    backgroundColor:'#00004d',
    height:'100%'
    
    },
    header:{
      backgroundColor:'#2D3579',  
      flexDirection: 'row',
      alignItems:'center',
      justifyContent:'space-between',
      borderBottomWidth: 2,
      paddingLeft:10,
      paddingRight: 10,
      borderColor:'#fff',
      height:'30%'
     },
  })