import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Home  from './src/Home/Home';
import Cadastro from './src/Cadastro/Cadastro';
import index   from './src/Login/index';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode ="none" initialRouteName ="index">        
        <Stack.Screen name="Home"component={Home}/>
        <Stack.Screen name="Cadastro"component={Cadastro}/>
        <Stack.Screen name="index"component={index}/>         
      </Stack.Navigator>     
    </NavigationContainer>
  );
}


